using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly BancoContext _context;

        public VendaController(BancoContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult Create(Venda venda)
        {
  
          venda.Status = EnumStatusVenda.AguardandoPagamento;  

        _context.Add(venda);
        _context.SaveChanges();

            return CreatedAtAction(nameof(ObterPorId), new { id = venda.Id }, venda);
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var venda = _context.Vendas.Find(id);

            if (venda == null)
                return NotFound();

            return Ok(venda);    
        }

        [HttpPut("{id}")]
        public IActionResult Atualizar(int id, EnumStatusVenda status)
        {
            var venda = _context.Vendas.Find(id);

            if (venda is null)
                return NotFound();

            if(StatusTeste(venda.Status, status))
            {
                venda.Status = status;

                _context.Vendas.Update(venda);
                _context.SaveChanges();

                return Ok(venda);
            }
            else
            {
                return BadRequest("Transição de Status Inválida!");
            }                  
        }

        private bool StatusTeste(EnumStatusVenda StatusAnterior, EnumStatusVenda StatusNovo)
        {
            //De: Aguardando pagamento Para: Pagamento Aprovado
            //De: Aguardando pagamento Para: Cancelada
            if (StatusAnterior == EnumStatusVenda.AguardandoPagamento
                && (StatusNovo == EnumStatusVenda.PagamentoAprovado || StatusNovo == EnumStatusVenda.Cancelada))
            {
                return true;
            }
            //De: Pagamento Aprovado Para: Enviado para Transportadora
            //De: Pagamento Aprovado Para: Cancelada
            else if (StatusAnterior == EnumStatusVenda.PagamentoAprovado &&
                (StatusNovo == EnumStatusVenda.EnviadoParaTransportadora || StatusNovo == EnumStatusVenda.Cancelada))
            {
                return true;
            }
            //De: Enviado para Transportador Para: Entregue
            else if (StatusAnterior == EnumStatusVenda.EnviadoParaTransportadora &&
               StatusNovo == EnumStatusVenda.Entregue)
            {
                return true;
            }
            else
            {
                return false;
            }

        }  
    }   
}    
